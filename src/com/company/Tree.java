package com.company;

import java.util.*;

/**
 * Created by Vitek on 4. 5. 2016.
 */
public class Tree {
    int value;
    List<Tree> children = new ArrayList<>();

    public Tree(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public int getLevel(){
        return children.size();
    }

    public int getSize(){
        int i = 0;
        for (Tree child: children) {
            i++;
            i+=child.getSize();
        }
        return i;
    }

    public void add(Tree node){
        children.add(node);
    }

    public int getMinValue(){
        int min = value;
        for (Tree child: children) {
            if (child.getValue()<min) min = child.getValue();
            if (child.getMinValue()<min) min = child.getMinValue();
        }
        return min;
    }

    public int getMaxValue(){
        int max = value;
        for (Tree child: children) {
            if (child.getValue()>max) max = child.getValue();
            if (child.getMaxValue()<max) max = child.getMaxValue();
        }
        return max;
    }

    public int getMaxLevel(){
        int max = children.size();
        for (Tree child: children) {
            if (children.size()>max) max = children.size();
            if (child.getMaxLevel()>max) max = child.getMaxLevel();
        }
        return max;
    }

    public int getHeight(){
        int max = 0;
        for (Tree child: children) {
            max++;
            if(child.getHeight()>max) max = child.getHeight();
        }
        return max;
    }
}
